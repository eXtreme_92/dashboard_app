function AppViewModel() {


    L.mapbox.accessToken = '';
    var map;

    var self = this;
    var url = "";




    self.initialization = function() {

        $.getJSON(url, function (data) {

            $.each(data.features, function (index, elem) {
                var flag = true;
                self.data.push(elem);
                if (self.categories().length == 0) {
                    self.categories.push(elem);
                } else {
                    ko.utils.arrayForEach(self.categories(), function (elem1) {

                        if ((elem.properties['Kategoria produktu'] !== elem1.properties['Kategoria produktu'])) {

                        }
                        else {
                            flag = false;
                            return false;
                        }
                    });
                    if (flag == true) {
                        self.categories.push(elem);
                    }
                }

                if (index == data.features.length - 1) {
                    self.chosen = self.categories()[0];
                    self.dataByCategory(self.chosen);
                    map = L.mapbox.map('map', 'mapbox.streets').setView([52, 20], 5);
                }
            });

        });

    }

    function getColor(zysk) {
        var color = null;
        $.each(self.coldwarm, function(index,el){
            if(index < self.coldwarm.length-1) {
                if (zysk <= el.gain) {
                    color = el.color;
                    return false;
                }
            } else {
                if (zysk > el.gain) {
                    color = el.color;
                    return false;
                }
            }
        });
        return color;
    }


    function getSize(size) {
        var sz = null;
        $.each(self.sizes, function(index,el){
            if(index < self.sizes.length-1) {
                if (size <= el.sale) {
                    sz = el.size;
                    return false;
                }
            } else {
                if (size > el.sale) {
                    sz = el.size;
                    return false;
                }
            }
        });
        return sz;
    }


    self.draw_map = function() {

        self.coldwarm = [
            {
                color: '#9933ff',
                gain: 500
            },
            {
                color: '#9933cc',
                gain: 1000
            },
            {
                color: '#993399',
                gain: 1500
            },
            {
                color: '#993366',
                gain: 2000
            },
            {
                color: '#993333',
                gain: 2500
            },
            {
                color: '#993300',
                gain: 2500
            }
        ];



        self.sizes = [
            {
                size: 'small',
                sale: 10000
            },
            {
                size: 'medium',
                sale: 20000
            },
            {
                size: 'large',
                sale: 30000
            }
        ];



        var markers = new L.MarkerClusterGroup();



        for (var i = 0; i < self.data_chosen.length; i++) {
            var a = self.data_chosen[i];
            var marker = L.marker(new L.LatLng(a.geometry.coordinates[1], a.geometry.coordinates[0]), {  //y , x
                icon: L.mapbox.marker.icon({'marker-symbol': '1', 'marker-color': getColor(parseFloat(a.properties["Zysk"])), 'marker-size': getSize(parseFloat(a.properties["Sprzedaż"]))})

            });
            marker.bindPopup('Sprzedaż: ' + a.properties["Sprzedaż"] +', Zysk: ' + a.properties["Zysk"]);
            markers.addLayer(marker);
        }


        map.addLayer(markers);
    }




    //category chosen by clicking on radio button
    self.chosen = ko.observable();

    //data loaded from server
    self.data = ko.observableArray();

    //array of unique categories only
    self.categories = ko.observableArray();

    self.loading = ko.observable(true);


    //data matching the category chosen
    self.data_chosen = ko.observableArray();


    self.gain_range = ko.observable({
        min: ko.observable(-10000000),
        max: ko.observable(10000000),
        value: ko.observable(-10000000)
    });


    self.gain_range_to = ko.observable({
        min: ko.observable(-10000000),
        max: ko.observable(10000000),
        value: ko.observable(10000000)
    });


    self.date_range = ko.observable({
        min: ko.observable(-10000000000000000000),
        max: ko.observable(10000000000000000000),
        value: ko.observable(-10000000000000000000)
    });

    self.date_range_to = ko.observable({
        min: ko.observable(-10000000000000000000),
        max: ko.observable(10000000000000000000),
        value: ko.observable(10000000000000000000)
    });





    self.getVal = ko.computed(function () {
        if(self.date_range().value() > self.date_range_to().value()) {
            self.date_range().value(self.date_range_to().value());
        }

        //console.log(self.date_range().value());
        var month = parseInt((new Date(parseInt(self.date_range().value()))).getUTCMonth()+1).toString();
        var day = parseInt((new Date(parseInt(self.date_range().value()))).getUTCDate()+0).toString();
        return ""+((day.length > 1) ? day : "0" + day) + "-" + ((month.length > 1) ? month : "0" + month) + "-" + (new Date(parseInt(self.date_range().value()))).getUTCFullYear();
    });





    self.getValTo = ko.computed(function () {
        var month = parseInt((new Date(parseInt(self.date_range_to().value()))).getUTCMonth()+1).toString();
        var day = parseInt((new Date(parseInt(self.date_range_to().value()))).getUTCDate()+0).toString();
        return ""+((day.length > 1) ? day : "0" + day) + "-" + ((month.length > 1) ? month : "0" + month) + "-" + (new Date(parseInt(self.date_range_to().value()))).getUTCFullYear();
    });



    self.getGainVal = ko.computed(function () {
        if(parseInt(self.gain_range().value()) > parseInt(self.gain_range_to().value())) {
            self.gain_range().value(self.gain_range_to().value());
        }

        return self.gain_range().value();
    });

    self.getGainValTo = ko.computed(function () {
        return self.gain_range_to().value();
    });

    var cat_helper;

    self.initialization();



    self.updatedrange = function() {
        self.dataByCategory(cat_helper, true);
    }


    //category button click handler
    self.dataByCategory = function(cat,init){

        //self.loading(true);

           setTimeout(function() {

               if(init==true) {
                   cat = cat_helper;
               } else {
                   cat_helper = cat;
               }

               self.point_by_regions([]);
               self.trend_by_regions([]);

               self.chosen = cat;


               if(init != true) {

                   self.gain_range().min(-10000000);
                   self.gain_range().max(10000000);
                   self.gain_range().value(-10000000);

                   self.gain_range_to().min(-10000000);
                   self.gain_range_to().max(10000000);
                   self.gain_range_to().value(10000000);

                   self.date_range().min(-10000000000000000000);
                   self.date_range().max(10000000000000000000);
                   self.date_range().value(-10000000000000000000);

                   self.date_range_to().min(-10000000000000000000);
                   self.date_range_to().max(10000000000000000000);
                   self.date_range_to().value(10000000000000000000);
               }

               self.data_chosen = ko.utils.arrayFilter(self.data(), function (el) {
                   return (
                   el.properties['Kategoria produktu'] == self.chosen.properties['Kategoria produktu']
                   && parseInt((new Date(el.properties['Data zamówienia'] + "T00:00:00Z")).getTime()) >= parseInt(self.date_range().value())
                   && parseInt((new Date(el.properties['Data zamówienia'] + "T00:00:00Z")).getTime()) <= parseInt(self.date_range_to().value())
                   && parseFloat(el.properties['Zysk']) >= parseFloat(self.gain_range().value())
                   && parseFloat(el.properties['Zysk']) <= parseFloat(self.gain_range_to().value())
                   );
               });


                if(self.data_chosen.length == 0) {
                    self.generate_point_chart(point_chart_params, true);
                    self.generate_trend_chart(trend_chart_params, true);
                    self.generate_product_chart(product_chart_params, true);
                    $('#map').html('');
                    return;
                }




               //redraw map

               $('#map').parent().each(function() {
                   //get child canvas id
                   childmapId = $(this).find("#map").attr('id');
                   //remove canvas
                   $('#' + childmapId).remove();
                   // append new canvas to the parent again
                   $(this).append("<div class=\"col-md-12\" id=\""+childmapId+"\" style=\"height:600px;\">");
               });

               map = L.mapbox.map('map', 'mapbox.streets').setView([52, 20], 5);
               self.draw_map();




               var legend = L.control({position: 'bottomright'});

               legend.onAdd = function (map) {

                   var gr = self.coldwarm.map(function(el) { return el.gain });

                   var div = L.DomUtil.create('div', 'info legend'),
                       grades = gr,
                       labels = [];

                   // loop through our density intervals and generate a label with a colored square for each interval

                   div.innerHTML = "<b>ZYSK</b><br/>";

                   for (var i in grades) {
                       if(i < grades.length-1) {
                           div.innerHTML += '<i style="background:' + self.coldwarm[i].color + '"></i> <= ' + grades[i] + '<br/>';
                       } else {
                           div.innerHTML += '<i style="background:' + self.coldwarm[i].color + '"></i> > ' + grades[i] + '<br/>';
                       }
                   }

                   return div;
               };

               legend.addTo(map);



               var datasetPrototype = {
                    responsive: true
               }


               var point_chart_params = {
                   data_labels: [],
                   config: {
                       type: 'line',
                       options: {
                           responsive: true,
                           maintainAspectRatio: false,
                           scales: {
                               xAxes: [{
                                   display: true,
                                   scaleLabel: {
                                       display: true,
                                       labelString: 'Sprzedaż'
                                   }
                               }],
                               yAxes: [{
                                   display: true,
                                   scaleLabel: {
                                       display: true,
                                       labelString: 'Zysk'
                                   }
                               }]
                           }
                       },
                       data: {
                           labels: [],
                           datasets: []
                       }
                   }
               };


               var trend_chart_params = {
                   data_labels: [],
                   config: {
                       type: 'line',
                       options: {
                           responsive: true,
                           maintainAspectRatio: false,
                           scales: {
                               xAxes: [{
                                   display: true,
                                   scaleLabel: {
                                       display: true,
                                       labelString: 'Data zamówienia'
                                   }
                               }],
                               yAxes: [{
                                   display: true,
                                   scaleLabel: {
                                       display: true,
                                       labelString: 'Sprzedaż'
                                   }
                               }]
                           }
                       },
                       data: {
                           labels: [],
                           datasets: []
                       }
                   }
               };


               var product_chart_params = {
                   config: {
                       type: 'bar',
                       options: {
                           responsive: true,
                           maintainAspectRatio: false,
                           legend: {
                               display: false
                           },
                           scales: {
                               xAxes: [{
                                   display: true,
                                   scaleLabel: {
                                       display: true,
                                       labelString: 'Region'
                                   }
                               }],
                               yAxes: [{
                                   display: true,
                                   scaleLabel: {
                                       display: true,
                                       labelString: 'Sprzedaż'
                                   }
                               }]
                           }
                       },
                       data: {
                           labels: [],
                           datasets: [{
                               label: 'Sprzedaż',
                               data: [],
                               backgroundColor: []
                           }]
                       }
                   }
               };


               var hash = {};
               var hash1 = {};

               ko.utils.arrayForEach(self.data_chosen, function (elem) {

                   point_chart_params.data_labels.push({
                       region: elem.properties['Region'],
                       label: Math.floor(parseFloat(elem.properties['Sprzedaż'])),
                       data: parseFloat(elem.properties['Zysk'])
                   });
                   /*
                   trend_chart_params.data_labels.push({
                       region: elem.properties['Region'],
                       label: new Date(elem.properties['Data zamówienia'] + "T00:00:00Z"),
                       data: parseFloat(elem.properties['Sprzedaż'])
                   });
*/

                   if (!hash[elem.properties['Region']]) {
                       hash[elem.properties['Region']] = {sale: parseFloat(elem.properties['Sprzedaż']), gain: parseFloat(elem.properties['Zysk'])};
                   }
                   else {
                       hash[elem.properties['Region']].sale += parseFloat(elem.properties['Sprzedaż']);
                       hash[elem.properties['Region']].gain += parseFloat(elem.properties['Zysk']);
                   }


                   if (!hash1[elem.properties['Region']+'|'+elem.properties['Data zamówienia']]) {
                       hash1[elem.properties['Region']+'|'+elem.properties['Data zamówienia']] = { region: elem.properties['Region'], data_zamowienia: elem.properties['Data zamówienia'], sprzedaz : parseFloat(elem.properties['Sprzedaż'])};
                   }
                   else {
                       hash1[elem.properties['Region']+'|'+elem.properties['Data zamówienia']].sprzedaz += parseFloat(elem.properties['Sprzedaż']);
                   }


               });

               $.each(hash1,function(index, el) {
                   trend_chart_params.data_labels.push({
                       region: hash1[index].region,
                       label: new Date(hash1[index].data_zamowienia + "T00:00:00Z"),
                       data: parseFloat(hash1[index].sprzedaz)
                   });
               });



               function compare(which,lbldata) {
                   return function (a, b) {
                       switch (which) {
                           case 'date':
                           {
                               if (a.label.getTime() < b.label.getTime())
                                   return -1;
                               else if (a.label.getTime() > b.label.getTime())
                                   return 1;
                               else
                                   return 0;
                           }

                           case 'number':
                           {
                               switch(lbldata) {
                                   case 'label':
                                   {
                                       if (a.label < b.label)
                                           return -1;
                                       else if (a.label > b.label)
                                           return 1;
                                       else
                                           return 0;
                                   }
                                   case 'data': {
                                       if (a.data < b.data)
                                           return -1;
                                       else if (a.data > b.data)
                                           return 1;
                                       else
                                           return 0;
                                   }
                               }
                           }

                       }
                   }

               }



               point_chart_params.data_labels.sort(compare('number','data'));
               trend_chart_params.data_labels.sort(compare('date'));


               /* point chart */


               var labels_data_temp = [];
               var data_counter = 0;

               var labels_data_length = point_chart_params.data_labels.length;

               if(init!=true) {
               self.gain_range().min(new Number(point_chart_params.data_labels[0].data));
               self.gain_range().max(new Number(point_chart_params.data_labels[labels_data_length - 1].data));
                   self.gain_range().value(new Number(point_chart_params.data_labels[0].data));
               }

               if(init!=true) {
               self.gain_range_to().min(new Number(point_chart_params.data_labels[0].data));
               self.gain_range_to().max(new Number(point_chart_params.data_labels[labels_data_length - 1].data));
                   self.gain_range_to().value(new Number(point_chart_params.data_labels[labels_data_length - 1].data));
               }

               point_chart_params.data_labels.sort(compare('number','label'));

               for (var i = point_chart_params.data_labels[0].label; i <= point_chart_params.data_labels[labels_data_length - 1].label; i++) {

                   var fl = true;
                   var el_helper;
                   $.each(point_chart_params.data_labels, function (index, el) {
                       if (el.label == i) {
                           el_helper = el;
                           fl = false;
                           return false;
                       } else {

                       }
                   });
                   if (fl == false) {

                       labels_data_temp.push({
                           region: el_helper.region,
                           label: i,
                           data: point_chart_params.data_labels[data_counter].data
                       });

                       data_counter++;

                   } else {

                       labels_data_temp.push({region: null, label: "", data: null});
                   }

               }


               var colors = ['lightblue', 'darkblue', 'darkred', 'aqua', 'beige', 'black', 'red', 'green', 'blue', 'yellow', 'orange'];

               $.each(labels_data_temp, function (index, el) {
                   if (el.region != null) {

                       var index_helper = undefined;
                       $.each(self.point_by_regions(), function (index1, el1) {
                           if (el1.region == el.region) {
                               index_helper = index1;
                               return false;
                           } else {

                           }

                       });

                       if (index_helper != undefined) {
                           point_chart_params.config.data.datasets[index_helper].data.push(el.data);
                           for (var dataset in point_chart_params.config.data.datasets) {
                               if (index_helper != dataset) {
                                   point_chart_params.config.data.datasets[dataset].data.push(null);
                               }
                           }
                       } else {
                           self.point_by_regions.push({color: colors.pop(), region: el.region});
                           var new_dataset = Object.create(datasetPrototype);
                           var color = self.point_by_regions()[self.point_by_regions().length - 1].color;
                           //new_dataset.constructor(undefined, color, "rgba(220,220,220,0.7)");
                           new_dataset.pointBackgroundColor = color;
                           new_dataset.fill = true;
                           new_dataset.backgroundColor = color;
                           new_dataset.label = "Zysk w " + el.region;
                           new_dataset.data = [];

                           point_chart_params.config.data.datasets.push(new_dataset);
                           for (var nulls = 0; nulls < index; nulls++) {
                               point_chart_params.config.data.datasets[self.point_by_regions().length - 1].data.push(null);
                           }
                           for (var dataset in point_chart_params.config.data.datasets) {
                               if (dataset != self.point_by_regions().length - 1) {
                                   point_chart_params.config.data.datasets[dataset].data.push(null);
                               }
                           }
                           point_chart_params.config.data.datasets[self.point_by_regions().length - 1].data.push(el.data);
                       }
                   } else {
                       for (var dataset in point_chart_params.config.data.datasets) {
                           point_chart_params.config.data.datasets[dataset].data.push(null);
                       }
                   }
               });


               point_chart_params.config.data.labels = labels_data_temp.map(function (el) {
                   return el.label
               });


               /* trend chart */

               labels_data_temp = [];
               data_counter = 0;

               labels_data_length = trend_chart_params.data_labels.length;

               if(init!=true) {
               self.date_range().min(trend_chart_params.data_labels[0].label.getTime());
               self.date_range().max(trend_chart_params.data_labels[labels_data_length - 1].label.getTime());
                   self.date_range().value(trend_chart_params.data_labels[0].label.getTime());
               }

               if(init!=true) {
               self.date_range_to().min(trend_chart_params.data_labels[0].label.getTime());
               self.date_range_to().max(trend_chart_params.data_labels[labels_data_length - 1].label.getTime());
                   self.date_range_to().value(trend_chart_params.data_labels[labels_data_length - 1].label.getTime());
               }

               for (var i = trend_chart_params.data_labels[0].label.getTime(); i <= trend_chart_params.data_labels[labels_data_length - 1].label.getTime(); i += 86400000) {

                   var fl = true;
                   var el_helper;
                   $.each(trend_chart_params.data_labels, function (index, el) {
                       if (el.label.getTime() == i) {
                           el_helper = el;
                           fl = false;
                           return false;
                       } else {

                       }
                   });
                   if (fl == false) {

                       labels_data_temp.push({
                           region: el_helper.region,
                           label: (new Date(i)).getUTCDate() + "-" + parseInt((new Date(i)).getUTCMonth() + 1) + "-" + (new Date(i)).getUTCFullYear(),
                           data: trend_chart_params.data_labels[data_counter].data
                       });

                       data_counter++;

                   } else {

                       labels_data_temp.push({region: null, label: "", data: null});
                   }

               }


               colors = ['lightblue', 'darkblue', 'darkred', 'aqua', 'beige', 'black', 'red', 'green', 'blue', 'yellow', 'orange'];

               $.each(labels_data_temp, function (index, el) {
                   if (el.region != null) {

                       var index_helper = undefined;
                       var color_helper = null;
                       $.each(self.trend_by_regions(), function (index1, el1) {
                           if (el1.region == el.region) {
                               index_helper = index1;
                               color_helper = el1.color;
                               return false;
                           } else {

                           }

                       });

                       if (index_helper != undefined) {
                           trend_chart_params.config.data.datasets[index_helper].data.push(el.data);
                           //trend_chart_params.config.data.datasets[index_helper].pointBackgroundColor.push(color_helper);
                           trend_chart_params.config.data.datasets[index_helper].pointBackgroundColor.push("rgba(0,0,0,1)");


                           for (var dataset in trend_chart_params.config.data.datasets) {
                               if (index_helper != dataset) {
                                   trend_chart_params.config.data.datasets[dataset].data.push(null);
                                   trend_chart_params.config.data.datasets[dataset].pointBackgroundColor.push("rgba(255,255,255,0)");

                               }
                           }
                       } else {
                           self.trend_by_regions.push({color: colors.pop(), region: el.region});

                           var new_dataset = Object.create(datasetPrototype);

                           var color = self.trend_by_regions()[self.trend_by_regions().length - 1].color;
                           new_dataset.fill = true;
                           new_dataset.backgroundColor = color;
                           new_dataset.borderColor = 'rgba(255,255,255,0)';
                           new_dataset.borderWidth = '0px';
                           new_dataset.pointDotRadius = '1';
                           new_dataset.pointBorderColor = 'rgba(255,255,255,0)';
                           new_dataset.pointBorderWidth = '0px';
                           new_dataset.label = "Sprzedaż w " + el.region;
                           new_dataset.pointBackgroundColor = [];
                           new_dataset.data = [];

                           trend_chart_params.config.data.datasets.push(new_dataset);
                           for (var nulls = 0; nulls < index; nulls++) {
                               trend_chart_params.config.data.datasets[self.trend_by_regions().length - 1].data.push(null);
                               trend_chart_params.config.data.datasets[self.trend_by_regions().length - 1].pointBackgroundColor.push("rgba(255,255,255,0)");
                           }
                           for (var dataset in trend_chart_params.config.data.datasets) {
                               if (dataset != self.trend_by_regions().length - 1) {
                                   trend_chart_params.config.data.datasets[dataset].data.push(null);
                                   trend_chart_params.config.data.datasets[dataset].pointBackgroundColor.push("rgba(255,255,255,0)");
                               }
                           }
                           trend_chart_params.config.data.datasets[self.trend_by_regions().length - 1].data.push(el.data);
                           //trend_chart_params.config.data.datasets[self.trend_by_regions().length - 1].pointBackgroundColor.push(color);
                           trend_chart_params.config.data.datasets[self.trend_by_regions().length - 1].pointBackgroundColor.push("rgba(255,255,255,0)");

                       }
                   } else {
                       for (var dataset in trend_chart_params.config.data.datasets) {
                           trend_chart_params.config.data.datasets[dataset].data.push(null);
                           trend_chart_params.config.data.datasets[dataset].pointBackgroundColor.push("rgba(255,255,255,0)");
                       }
                   }
               });


               trend_chart_params.config.data.labels = labels_data_temp.map(function (el) {
                   return el.label
               });

               var nulls = [];
               for (var dataset in trend_chart_params.config.data.datasets) {
                   var nulls_helper = [];
                   $.each(trend_chart_params.config.data.datasets[dataset].data, function (index, data) {
                       if (data != null) {
                           nulls_helper.push({index:index, data:data});
                       }
                   });
                   nulls.push(nulls_helper);
               }

               for(var obj in nulls) {
                   for(var i = 0, j = i + 1; i < nulls[obj].length - 1; i++) {

                       var spanning_points = Math.abs(nulls[obj][j].index - nulls[obj][i].index);
                       var diff = Math.abs(nulls[obj][j].data - nulls[obj][i].data);
                       var interval = diff/spanning_points;

                       for(var x = 1; x < spanning_points; x++){
/*
                           if(nulls[obj][j].data < nulls[obj][i].data) {
*/
                               trend_chart_params.config.data.datasets[obj].data[nulls[obj][i].index + x] =
                                   trend_chart_params.config.data.datasets[obj].data[nulls[obj][i].index + x - 1] + interval;
/*                           } else {
                               trend_chart_params.config.data.datasets[obj].data[nulls[obj][i].index + x] =
                                   trend_chart_params.config.data.datasets[obj].data[nulls[obj][i].index + x - 1] - interval;
                           }
                           */
                       }



                   }
               }





                   /* product chart */

               for (var el in hash) {

                   if (hash.hasOwnProperty(el)) {
                       product_chart_params.config.data.labels.push(el);
                       product_chart_params.config.data.datasets[0].data.push(hash[el].sale);
                       product_chart_params.config.data.datasets[0].backgroundColor.push(getColor(hash[el].gain));
                   }
               }

//




               $('canvas').parent().each(function() {
                   //get child canvas id
                   childCanvasId = $(this).find("canvas").attr('id');
                   //remove canvas
                   $('#' + childCanvasId).remove();
                   // append new canvas to the parent again
                   $(this).append('<canvas id="' + childCanvasId + '" style="height:600px; width:100%;"></canvas>');
               });


            self.generate_point_chart(point_chart_params);
            self.generate_trend_chart(trend_chart_params);
            self.generate_product_chart(product_chart_params);

               self.loading(false);
        },0);

        return true;
    };

//formatted value of prod input
    self.prod = function(index) {
        return "prod_" + index();
    };

//formatted value of date input
    self.date = function(index) {
        return "date_" + index();
    };

//formatted value of gain input
    self.gain = function(index) {
        return "gain_" + index();
    };






    self.generate_point_chart = function(parameters,isempty) {

        if(isempty == undefined) {
            var ctx = $("#point_chart").get(0).getContext("2d");
            ctx.clearRect(0, 0, $("#point_chart").width(), $("#point_chart").height());

            var point_chart;
            if (point_chart == undefined) {
                point_chart = new Chart(ctx, parameters.config);
            } else {
                point_chart.destroy();
                point_chart = new Chart(ctx, parameters.config);
            }
        } else {
            var ctx = $("#point_chart").get(0).getContext("2d");
            ctx.clearRect(0, 0, $("#point_chart").width(), $("#point_chart").height());
        }
    }




    self.generate_trend_chart = function(parameters,isempty) {

        if(isempty == undefined) {
        var ctx = $("#trend_chart").get(0).getContext("2d");
        ctx.clearRect(0,0,$("#trend_chart").width(),$("#point_chart").height());

        var trend_chart;
        if(trend_chart == undefined) {
            trend_chart = new Chart(ctx, parameters.config);
        } else {
            trend_chart.destroy();
            trend_chart = new Chart(ctx, parameters.config);
        }
    } else {
            var ctx = $("#trend_chart").get(0).getContext("2d");
            ctx.clearRect(0,0,$("#trend_chart").width(),$("#point_chart").height());
        }
    }


    self.generate_product_chart = function(parameters,isempty) {

        if(isempty == undefined) {
            var ctx = $("#product_chart").get(0).getContext("2d");
            ctx.clearRect(0, 0, $("#product_chart").width(), $("#point_chart").height());
     //       var product_chart = new Chart(ctx, parameters.config);

            var product_chart;
            if (product_chart == undefined) {
                product_chart = new Chart(ctx, parameters.config);
            } else {
                product_chart.destroy();
                product_chart = new Chart(ctx, parameters.config);
            }
        } else {
            var ctx = $("#product_chart").get(0).getContext("2d");
            ctx.clearRect(0, 0, $("#product_chart").width(), $("#point_chart").height());
        }
    }


    self.point_by_regions = ko.observableArray([]);
    self.trend_by_regions = ko.observableArray([]);


}


ko.applyBindings(new AppViewModel());

